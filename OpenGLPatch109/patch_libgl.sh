#!/bin/bash
cp libGL_mav.dylib libGL_mav_p.dylib
fat_offset64=`otool -f libGL_mav.dylib | grep -m1 offset | awk '{print $2}'`
for a in glGetObjectLabelEXT glInsertEventMarkerEXT glLabelObjectEXT glLabelObjectWithResponsibleProcessAPPLE glPopGroupMarkerEXT glPushGroupMarkerEXT glTextureBarrierNV; do printf '\xc3' | dd of=libGL_mav_p.dylib bs=1 count=1 conv=notrunc seek=$((0x$(otool -tvV libGL_mav.dylib| grep -A1 "_$a:$" | tail -n1 | cut -d "`printf '\t'`" -f1)+$fat_offset64)); done
fat_offset32=`otool -f libGL_mav.dylib | grep -m2 offset | tail -n1 | awk '{print $2}'`
for a in glGetObjectLabelEXT glInsertEventMarkerEXT glLabelObjectEXT glLabelObjectWithResponsibleProcessAPPLE glPopGroupMarkerEXT glPushGroupMarkerEXT glTextureBarrierNV; do printf '\xc3' | dd of=libGL_mav_p.dylib bs=1 count=1 conv=notrunc seek=$((0x$(otool -arch i386 -tvV libGL_mav.dylib| grep -A1 "_$a:$" | tail -n1 | cut -d "`printf '\t'`" -f1)+$fat_offset32)); done
rm -f libGL_mav_p.dylib.unsigned
unsign/unsign libGL_mav_p.dylib
rm -f libGL_mav_p.dylib 
mv libGL_mav_p.dylib.unsigned libGL_mav_p.dylib
mv libGL_mav_p.dylib OpenGL.framework/Versions/A/Libraries/libGL.dylib