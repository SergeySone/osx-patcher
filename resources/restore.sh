#!/bin/bash

parameters="${1}${2}${3}${4}${5}${6}${7}${8}${9}"

Escape_Variables()
{
	text_progress="\033[38;5;113m"
	text_success="\033[38;5;113m"
	text_warning="\033[38;5;221m"
	text_error="\033[38;5;203m"
	text_message="\033[38;5;75m"

	text_bold="\033[1m"
	text_faint="\033[2m"
	text_italic="\033[3m"
	text_underline="\033[4m"

	erase_style="\033[0m"
	erase_line="\033[0K"

	move_up="\033[1A"
	move_down="\033[1B"
	move_foward="\033[1C"
	move_backward="\033[1D"
}

Parameter_Variables()
{
	if [[ $parameters == *"-v"* || $parameters == *"-verbose"* ]]; then
		verbose="1"
		set -x
	fi
}

Path_Variables()
{
	script_path="${0}"
	directory_path="${0%/*}"
}

Input_Off()
{
	stty -echo
}

Input_On()
{
	stty echo
}

Output_Off()
{
	if [[ $verbose == "1" ]]; then
		"$@"
	else
		"$@" &>/dev/null
	fi
}

Check_Environment()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking system environment."${erase_style}

	if [ -d /Install\ *.app ]; then
		environment="installer"
	fi

	if [ ! -d /Install\ *.app ]; then
		environment="system"
	fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Checked system environment."${erase_style}
}

Check_Root()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking for root permissions."${erase_style}

	if [[ $environment == "installer" ]]; then
		root_check="passed"
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Root permissions check passed."${erase_style}
	else

		if [[ $(whoami) == "root" && $environment == "system" ]]; then
			root_check="passed"
			echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Root permissions check passed."${erase_style}
		fi

		if [[ ! $(whoami) == "root" && $environment == "system" ]]; then
			root_check="failed"
			echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- Root permissions check failed."${erase_style}
			echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool with root permissions."${erase_style}
			Input_On
			exit
		fi

	fi
}

Input_Model()
{
model_list="/     iMac4,1
/     iMac4,2
/     iMac5,1
/     iMac5,2
/     iMac6,1
/     MacBook2,1
/     MacBook3,1
/     MacBook4,1
/     MacBookAir1,1
/     MacBookPro2,1
/     MacBookPro2,2
/     Macmini1,1
/     Macmini2,1
/     MacPro1,1
/     MacPro2,1
/     Xserve1,1
/     Xserve2,1"

model_ati="iMac4,1
iMac5,1
MacBookPro2,1
MacBookPro2,2
MacPro1,1
MacPro2,1
Xserve1,1
Xserve2,1"

	model_detected="$(sysctl -n hw.model)"

	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Detecting model."${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Detected model as $model_detected."${erase_style}

	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What model would you like to use?"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input an model option."${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     1 - Use detected model"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     2 - Use manually selected model"${erase_style}

	Input_On
	read -e -p "$(date "+%b %d %H:%M:%S") / " model_option
	Input_Off

	if [[ $model_option == "1" ]]; then
		model="$model_detected"
		echo -e $(date "+%b %d %H:%M:%S") ${text_success}"+ Using $model_detected as model."${erase_style}
	fi

	if [[ $model_option == "2" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What model would you like to use?"${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input your model."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"$model_list"${erase_style}

		Input_On
		read -e -p "$(date "+%b %d %H:%M:%S") / " model_selected
		Input_Off

		model="$model_selected"
		echo -e $(date "+%b %d %H:%M:%S") ${text_success}"+ Using $model_selected as model."${erase_style}
	fi
}

Input_Volume()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What volume would you like to use?"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input a volume number."${erase_style}

	for volume_path in /Volumes/*; do
		volume_name="${volume_path#/Volumes/}"

		if [[ ! "$volume_name" == com.apple* ]]; then
			volume_number=$(($volume_number + 1))
			declare volume_$volume_number="$volume_name"

			echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     ${volume_number} - ${volume_name}"${erase_style} | sort
		fi

	done

	Input_On
	read -e -p "$(date "+%b %d %H:%M:%S") / " volume_number
	Input_Off

	volume="volume_$volume_number"
	volume_name="${!volume}"
	volume_path="/Volumes/$volume_name"
}

Check_Volume_Version()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking system version."${erase_style}
		
		volume_version="$(defaults read "$volume_path"/System/Library/CoreServices/SystemVersion.plist ProductVersion)"
		volume_version_short="$(defaults read "$volume_path"/System/Library/CoreServices/SystemVersion.plist ProductVersion | cut -c-5)"
		
		volume_build="$(defaults read "$volume_path"/System/Library/CoreServices/SystemVersion.plist ProductBuildVersion)"
	
		if [[ ${#volume_version} == "6" ]]; then
			volume_version_short="$(defaults read "$volume_path"/System/Library/CoreServices/SystemVersion.plist ProductVersion | cut -c-4)"
		fi
	
		if [[ $environment == "installer" ]]; then
			system_volume_version="$(defaults read /System/Library/CoreServices/SystemVersion.plist ProductVersion)"
			system_volume_version_short="$(defaults read /System/Library/CoreServices/SystemVersion.plist ProductVersion | cut -c-5)"
		
			system_volume_build="$(defaults read /System/Library/CoreServices/SystemVersion.plist ProductBuildVersion)"
	
			if [[ ${#volume_version} == "6" ]]; then
				system_volume_version_short="$(defaults read /System/Library/CoreServices/SystemVersion.plist ProductVersion | cut -c-4)"
			fi
		fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Checked system version."${erase_style}
}

Check_Volume_Support()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking system support."${erase_style}

	if [[ $volume_version_short == "10."[8-9] || $volume_version_short == "10.1"[0-1] ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ System support check passed."${erase_style}
	else
		echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- System support check failed."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool on a supported system."${erase_style}

		Input_On
		exit
	fi
}

Check_Volume_parrotgeek()
{
	if [ -e "$volume_path"/Library/LaunchAgents/com.parrotgeek* ]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_warning}"! Your system was patched by another patcher."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool on a clean system."${erase_style}

		Input_On
		exit
	fi
}

Clean_Volume()
{
	Output_Off rm -R "$volume_path"/Applications/Utilities/Brightness\ Slider.app

	Output_Off rm -R "$volume_path"/Applications/Utilities/NoSleep.app
	Output_Off rm -R "$volume_path"/System/Library/Extensions/NoSleep.kext
	Output_Off rm -R "$volume_path"/System/Library/PreferencePanes/NoSleep.prefPane
}

Restore_Volume()
{
	if [[ ! $volume_version_short == "10.8" ]] || [[ $volume_version_short == "10.8" && $model_ati == *$model* ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing boot.efi patch."${erase_style}

			chflags nouchg "$volume_path"/System/Library/CoreServices/boot.efi
			rm "$volume_path"/System/Library/CoreServices/boot.efi

		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed boot.efi patch."${erase_style}
	fi


	if [[ $volume_version_short == "10.8" && ! $model_ati == *$model* ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing 32-bit components patch."${erase_style}

			rm -R "$volume_path"/System/Library/CoreServices/BluetoothAudioAgent.app
			rm -R "$volume_path"/System/Library/CoreServices/Bluetooth\ Setup\ Assistant.app
			rm -R "$volume_path"/System/Library/CoreServices/Menu\ Extras/Bluetooth.menu
			rm -R "$volume_path"/System/Library/CoreServices/gmacheck

			rm -R "$volume_path"/System/Library/Extensions/Accusys6xxxx.kext
			rm -R "$volume_path"/System/Library/Extensions/acfs.kext
			rm -R "$volume_path"/System/Library/Extensions/acfsctl.kext
			rm -R "$volume_path"/System/Library/Extensions/ALF.kext
			rm -R "$volume_path"/System/Library/Extensions/Apple16X50Serial.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleAHCIPort.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleAPIC.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleAVBAudio.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleBacklight.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleBacklightExpert.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleBluetoothMultitouch.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleBMC.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleEFIRuntime.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleFileSystemDriver.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleFSCompressionTypeDataless.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleFSCompressionTypeZlib.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleFWAudio.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleGraphicsControl.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleGraphicsPowerManagement.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleHDA.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleHIDKeyboard.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleHIDMouse.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleHPET.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleHWSensor.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelFramebufferCapri.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelHD3000Graphics.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelHD3000GraphicsGA.plugin
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelHD3000GraphicsGLDriver.bundle
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelHD3000GraphicsVADriver.bundle
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelHD4000Graphics.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelHD4000GraphicsGA.plugin
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelHD4000GraphicsGLDriver.bundle
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelHD4000GraphicsVADriver.bundle
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelHDGraphics.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelHDGraphicsFB.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelHDGraphicsGA.plugin
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelHDGraphicsGLDriver.bundle
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelHDGraphicsVADriver.bundle
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelIVBVA.bundle
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelSNBGraphicsFB.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleIntelSNBVA.bundle
			rm -R "$volume_path"/System/Library/Extensions/AppleIRController.kext
			rm -R "$volume_path"/System/Library/Extensions/Apple_iSight.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleKeyStore.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleKeyswitch.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleLPC.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleLSIFusionMPT.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleMatch.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleMCCSControl.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleMCEDriver.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleMIDIFWDriver.plugin
			rm -R "$volume_path"/System/Library/Extensions/AppleMIDIIACDriver.plugin
			rm -R "$volume_path"/System/Library/Extensions/AppleMIDIRTPDriver.plugin
			rm -R "$volume_path"/System/Library/Extensions/AppleMIDIUSBDriver.plugin
			rm -R "$volume_path"/System/Library/Extensions/AppleMikeyHIDDriver.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleMultitouchDriver.kext
			rm -R "$volume_path"/System/Library/Extensions/ApplePlatformEnabler.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleRAID.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleRAIDCard.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleRTC.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleSDXC.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleSEP.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleSmartBatteryManager.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleSMBIOS.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleSMBusController.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleSMBusPCI.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleSMC.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleSMCLMU.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleSRP.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleStorageDrivers.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleThunderboltDPAdapters.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleThunderboltEDMService.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleThunderboltNHI.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleThunderboltPCIAdapters.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleThunderboltUTDM.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleTyMCEDriver.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleUpstreamUserClient.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleUSBAudio.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleUSBDisplays.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleUSBEthernetHost_32.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleUSBMultitouch.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleUSBTopCase.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleVADriver.bundle
			rm -R "$volume_path"/System/Library/Extensions/AppleWWANAutoEject.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleXsanFilter.kext
			rm -R "$volume_path"/System/Library/Extensions/ArcMSR.kext
			rm -R "$volume_path"/System/Library/Extensions/ATI2400Controller.kext
			rm -R "$volume_path"/System/Library/Extensions/ATI2600Controller.kext
			rm -R "$volume_path"/System/Library/Extensions/ATI3800Controller.kext
			rm -R "$volume_path"/System/Library/Extensions/ATI4600Controller.kext
			rm -R "$volume_path"/System/Library/Extensions/ATI4800Controller.kext
			rm -R "$volume_path"/System/Library/Extensions/ATI5000Controller.kext
			rm -R "$volume_path"/System/Library/Extensions/ATI6000Controller.kext
			rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX2000.kext
			rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX2000GA.plugin
			rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX2000GLDriver.bundle
			rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX2000VADriver.bundle
			rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX3000.kext
			rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX3000GA.plugin
			rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX3000GLDriver.bundle
			rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX3000VADriver.bundle
			rm -R "$volume_path"/System/Library/Extensions/ATTOCelerityFC.kext
			rm -R "$volume_path"/System/Library/Extensions/ATTOCelerityFC8.kext
			rm -R "$volume_path"/System/Library/Extensions/ATTOExpressPCI4.kext
			rm -R "$volume_path"/System/Library/Extensions/ATTOExpressSASHBA.kext
			rm -R "$volume_path"/System/Library/Extensions/ATTOExpressSASHBA2.kext
			rm -R "$volume_path"/System/Library/Extensions/ATTOExpressSASRAID.kext
			rm -R "$volume_path"/System/Library/Extensions/ATTOExpressSASRAID2.kext
			rm -R "$volume_path"/System/Library/Extensions/AudioAUUC.kext
			rm -R "$volume_path"/System/Library/Extensions/autofs.kext
			rm -R "$volume_path"/System/Library/Extensions/BJUSBLoad.kext
			rm -R "$volume_path"/System/Library/Extensions/BootCache.kext
			rm -R "$volume_path"/System/Library/Extensions/CalDigitHDProDrv.kext
			rm -R "$volume_path"/System/Library/Extensions/cd9660.kext
			rm -R "$volume_path"/System/Library/Extensions/cddafs.kext
			rm -R "$volume_path"/System/Library/Extensions/CellPhoneHelper.kext
			rm -R "$volume_path"/System/Library/Extensions/CoreStorage.kext
			rm -R "$volume_path"/System/Library/Extensions/Dont\ Steal\ Mac\ OS\ X.kext
			rm -R "$volume_path"/System/Library/Extensions/DSACL.ppp
			rm -R "$volume_path"/System/Library/Extensions/DSAuth.ppp
			rm -R "$volume_path"/System/Library/Extensions/DVFamily.bundle
			rm -R "$volume_path"/System/Library/Extensions/EAP-KRB.ppp
			rm -R "$volume_path"/System/Library/Extensions/EAP-RSA.ppp
			rm -R "$volume_path"/System/Library/Extensions/EAP-TLS.ppp
			rm -R "$volume_path"/System/Library/Extensions/EPSONUSBPrintClass.kext
			rm -R "$volume_path"/System/Library/Extensions/exfat.kext
			rm -R "$volume_path"/System/Library/Extensions/GeForce7xxx.kext
			rm -R "$volume_path"/System/Library/Extensions/GeForce7xxxGA.plugin
			rm -R "$volume_path"/System/Library/Extensions/GeForce7xxxVADriver.bundle
			rm -R "$volume_path"/System/Library/Extensions/GeForceGLDriver.bundle
			rm -R "$volume_path"/System/Library/Extensions/HighPointIOP.kext
			rm -R "$volume_path"/System/Library/Extensions/HighPointRR.kext
			rm -R "$volume_path"/System/Library/Extensions/HighPointRR644.kext
			rm -R "$volume_path"/System/Library/Extensions/hp_fax_io.kext
			rm -R "$volume_path"/System/Library/Extensions/hp_Inkjet1_io_enabler.kext
			rm -R "$volume_path"/System/Library/Extensions/IO80211Family.kext
			rm -R "$volume_path"/System/Library/Extensions/IOAccelerator2D.plugin
			rm -R "$volume_path"/System/Library/Extensions/IOAcceleratorFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOAHCIFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOATAFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOAudioFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOAVBFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOBDStorageFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOBluetoothFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOBluetoothHIDDriver.kext
			rm -R "$volume_path"/System/Library/Extensions/IOCDStorageFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IODVDStorageFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOFireWireAVC.kext
			rm -R "$volume_path"/System/Library/Extensions/IOFireWireFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOFireWireIP.kext
			rm -R "$volume_path"/System/Library/Extensions/IOFireWireSBP2.kext
			rm -R "$volume_path"/System/Library/Extensions/IOFireWireSerialBusProtocolTransport.kext
			rm -R "$volume_path"/System/Library/Extensions/IOGraphicsFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOHDIXController.kext
			rm -R "$volume_path"/System/Library/Extensions/IOHIDFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IONDRVSupport.kext
			rm -R "$volume_path"/System/Library/Extensions/IONetworkingFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOPCIFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOPlatformPluginFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOSCSIArchitectureModelFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOSCSIParallelFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOSerialFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOSMBusFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOStorageFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOStreamFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOSurface.kext
			rm -R "$volume_path"/System/Library/Extensions/IOThunderboltFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOTimeSyncFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOUSBAttachedSCSI.kext
			rm -R "$volume_path"/System/Library/Extensions/IOUSBFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOUSBMassStorageClass.kext
			rm -R "$volume_path"/System/Library/Extensions/IOUserEthernet.kext
			rm -R "$volume_path"/System/Library/Extensions/IOVideoFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/iPodDriver.kext
			rm -R "$volume_path"/System/Library/Extensions/JMicronATA.kext
			rm -R "$volume_path"/System/Library/Extensions/L2TP.ppp
			rm -R "$volume_path"/System/Library/Extensions/mcxalr.kext
			rm -R "$volume_path"/System/Library/Extensions/msdosfs.kext
			rm -R "$volume_path"/System/Library/Extensions/ntfs.kext
			rm -R "$volume_path"/System/Library/Extensions/NVDAGF100Hal.kext
			rm -R "$volume_path"/System/Library/Extensions/NVDAGK100Hal.kext
			rm -R "$volume_path"/System/Library/Extensions/NVDANV40HalG7xxx.kext
			rm -R "$volume_path"/System/Library/Extensions/NVDAResmanG7xxx.kext
			rm -R "$volume_path"/System/Library/Extensions/NVSMU.kext
			rm -R "$volume_path"/System/Library/Extensions/OSvKernDSPLib.kext
			rm -R "$volume_path"/System/Library/Extensions/PPP.kext
			rm -R "$volume_path"/System/Library/Extensions/PPPoE.ppp
			rm -R "$volume_path"/System/Library/Extensions/PPPSerial.ppp
			rm -R "$volume_path"/System/Library/Extensions/PPTP.ppp
			rm -R "$volume_path"/System/Library/Extensions/PromiseSTEX.kext
			rm -R "$volume_path"/System/Library/Extensions/Quarantine.kext
			rm -R "$volume_path"/System/Library/Extensions/Radius.ppp
			rm -R "$volume_path"/System/Library/Extensions/SM56KUSBAudioFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/SMARTLib.plugin
			rm -R "$volume_path"/System/Library/Extensions/smbfs.kext
			rm -R "$volume_path"/System/Library/Extensions/SMCMotionSensor.kext
			rm -R "$volume_path"/System/Library/Extensions/Sandbox.kext
			rm -R "$volume_path"/System/Library/Extensions/SoftRAID.kext
			rm -R "$volume_path"/System/Library/Extensions/System.kext
			rm -R "$volume_path"/System/Library/Extensions/TMSafetyNet.kext
			rm -R "$volume_path"/System/Library/Extensions/triggers.kext
			rm -R "$volume_path"/System/Library/Extensions/udf.kext
			rm -R "$volume_path"/System/Library/Extensions/webcontentfilter.kext
			rm -R "$volume_path"/System/Library/Extensions/webdav_fs.kext

			rm -R "$volume_path"/System/Library/Filesystems/AppleShare/afpfs.kext
			rm -R "$volume_path"/System/Library/Filesystems/AppleShare/asp_tcp.kext
			rm -R "$volume_path"/System/Library/Filesystems/hfs.fs
			rm -R "$volume_path"/System/Library/Filesystems/NetFSPlugins/afp.bundle

			rm -R "$volume_path"/System/Library/Frameworks/AppleShareClientCore.framework
			rm -R "$volume_path"/System/Library/Frameworks/CoreWiFi.framework
			rm -R "$volume_path"/System/Library/Frameworks/CoreWLAN.framework
			rm -R "$volume_path"/System/Library/Frameworks/IOBluetooth.framework
			rm -R "$volume_path"/System/Library/Frameworks/NetFS.framework

			rm -R "$volume_path"/System/Library/LaunchAgents/com.apple.Dock.plist

			rm -R "$volume_path"/System/Library/PreferencePanes/Bluetooth.prefPane
			rm -R "$volume_path"/System/Library/PreferencePanes/Mouse.prefPane

			rm -R "$volume_path"/System/Library/PrivateFrameworks/Apple80211.framework
			rm -R "$volume_path"/System/Library/PrivateFrameworks/AppleProfileFamily.framework
			rm -R "$volume_path"/System/Library/PrivateFrameworks/CoreWLANKit.framework
			rm -R "$volume_path"/System/Library/PrivateFrameworks/EAP8021X.framework

			rm -R "$volume_path"/System/Library/SystemConfiguration//Apple80211Monitor.bundle
			rm -R "$volume_path"/System/Library/SystemConfiguration//EAPOLController.bundle

			rm -R "$volume_path"/System/Library/SystemProfiler/SPBluetoothReporter.spreporter
			rm -R "$volume_path"/System/Library/SystemProfiler/SPDisplaysReporter.spreporter

			rm -R "$volume_path"/System/Library/UserEventPlugins/AirPortUserAgent.plugin
			rm -R "$volume_path"/System/Library/UserEventPlugins/BluetoothUserAgent-Plugin.plugin

			rm -R "$volume_path"/usr/libexec/airportd
			rm -R "$volume_path"/usr/libexec/wifid
			rm -R "$volume_path"/usr/libexec/wps

			rm -R "$volume_path"/usr/sbin/blued
			rm -R "$volume_path"/usr/sbin/bnepd
			rm -R "$volume_path"/usr/sbin/mDNSResponder

			rm -R "$volume_path"/mach_kernel

		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed 32-bit components patch."${erase_style}
	fi


	if [[ $volume_version_short == "10.11" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing input drivers patch."${erase_style}
			
			rm -R "$volume_path"/System/Library/Extensions/AppleHIDMouse.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleIRController.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleTopCase.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleUSBMultitouch.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleUSBTopCase.kext
			rm -R "$volume_path"/System/Library/Extensions/IOBDStorageFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOBluetoothFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOBluetoothHIDDriver.kext
			rm -R "$volume_path"/System/Library/Extensions/IOSerialFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOUSBFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOUSBHostFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOUSBMassStorageClass.kext
		
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed input drivers patch."${erase_style}
	fi


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing graphics drivers patch."${erase_style}
		
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMA950.kext
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMA950GA.plugin
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMA950GLDriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMA950VADriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100.kext
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100FB.kext
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100GA.plugin
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100GLDriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100VADriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelIntegratedFramebuffer.kext
		rm -R "$volume_path"/System/Library/Extensions/ATI1300Controller.kext
		rm -R "$volume_path"/System/Library/Extensions/ATI1600Controller.kext
		rm -R "$volume_path"/System/Library/Extensions/ATI1900Controller.kext
		rm -R "$volume_path"/System/Library/Extensions/ATIFramebuffer.kext
		rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX1000.kext
		rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX1000GA.plugin
		rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX1000GLDriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX1000VADriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/ATISupport.kext
		rm -R "$volume_path"/System/Library/Extensions/GeForce.kext
		rm -R "$volume_path"/System/Library/Extensions/GeForce7xxxGLDriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/GeForceGA.plugin
		rm -R "$volume_path"/System/Library/Extensions/GeForceVADriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/NVDANV50Hal.kext
		rm -R "$volume_path"/System/Library/Extensions/NVDAResman.kext
	
		if [[ $volume_version_short == "10.8" ]] || [[ $volume_version_short == "10.9" && $model_ati == *$model* ]]; then
			rm -R "$volume_path"/System/Library/Extensions/GeForce7xxx.kext 
			rm -R "$volume_path"/System/Library/Extensions/GeForce7xxxGA.plugin 
			rm -R "$volume_path"/System/Library/Extensions/GeForce7xxxVADriver.bundle 
			rm -R "$volume_path"/System/Library/Extensions/GeForceGLDriver.bundle 
			rm -R "$volume_path"/System/Library/Extensions/NVDAGF100Hal.kext 
			rm -R "$volume_path"/System/Library/Extensions/NVDAGK100Hal.kext 
			rm -R "$volume_path"/System/Library/Extensions/NVDANV40HalG7xxx.kext 
			rm -R "$volume_path"/System/Library/Extensions/NVDAResmanG7xxx.kext 

			rm -R "$volume_path"/System/Library/Frameworks/OpenCL.framework
			rm -R "$volume_path"/System/Library/Frameworks/OpenGL.framework
		fi

		if [[ $volume_version_short == "10.9" && $model_ati == *$model* ]]; then
			rm -R "$volume_path"/System/Library/Frameworks/CoreGraphics.framework
			rm -R "$volume_path"/System/Library/Frameworks/QuartzCore.framework
			rm -R "$volume_path"/System/Library/PrivateFrameworks/Slideshows.framework
		fi 

		if [[ ! $volume_version_short == "10.8" && ! $model_ati == *$model* ]]; then
			rm "$volume_path"/Library/Preferences/com.apple.PowerManagement.plist

			defaults delete "$volume_path"/System/Library/LaunchDaemons/com.apple.WindowServer.plist Nice
		fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed graphics drivers patch."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing audio drivers patch."${erase_style}
		
		rm -R "$volume_path"/System/Library/Extensions/AppleHDA.kext
		rm -R "$volume_path"/System/Library/Extensions/IOAudioFamily.kext
	
	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed audio drivers patch."${erase_style}


	if [[ $volume_version_short == "10.11" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing System Integrity Protection."${erase_style}

			rm -R "$volume_path"/System/Library/Extensions/SIPManager.kext

		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed System Integrity Protection."${erase_style}
	fi
}

End()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Thank you for using OS X Patcher."${erase_style}
	
	Input_On
	exit
}

Input_Off
Escape_Variables
Parameter_Variables
Path_Variables
Check_Environment
Check_Root
Input_Model
Input_Volume
Check_Volume_Version
Check_Volume_Support
Check_Volume_parrotgeek
Clean_Volume
Restore_Volume
End