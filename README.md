[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# OS X Patcher
OS X Patcher is an [open source](#license) command line tool for running OS X Mountain Lion, OS X Mavericks, OS X Yosemite, and OS X El Capitan on unsupported Macs

## Linux makes Macs better
If you can, you shoud really consider [using Linux](https://julianfairfax.gitlab.io/blog/20221029/linux-makes-macs-better) instead of this patcher.

## Software Updates
When installing software updates on your unsupported Mac, this patcher's patches may be overwritten by the new versions of system files that are being installed. It's highly recommended to have a patched installer to patch your system after the update.

## Contributors
I'd like to the thank the following people, and many others, for their research, help, and inspiration.
- [andyvand](https://github.com/andyvand) for the efilipo tool (source code in [this folder](https://github.com/andyvand/macosxbootloader/tree/master/efilipo))
- [parrotgeek1](https://github.com/parrotgeek1) for the Mountain Lion gmacheck (source code in [the gmacheck.c file](/gmacheck.c) and Macmini1,1/2,1 AirPort patches, Mountain Lion to Mavericks graphics acceleration patches (source code in [the OpenGLPatch108](/OpenGLPatch108/) and [the OpenGLPatch109](/OpenGLPatch109/) folders), El Capitan SIPManager patch (source code in [the SIPManager folder](/SIPManager)), and for invaluable help in starting this project
- [TMRJIJ](https://github.com/TMRJIJ), for invaluable help in starting this project
- [Piker Alpha](https://github.com/Piker-Alpha) for the 32-bit boot.efi (source code in [this repo](https://github.com/Piker-Alpha/macosxbootloader))

## Supported Macs

### MacBooks
- MacBook2,1
- MacBook3,1
- MacBook4,1

### MacBook Airs
- MacBookAir1,1

### MacBook Pros
- MacBookPro2,1
- MacBookPro2,2

### iMacs
- iMac4,1
- iMac4,2
- iMac5,1
- iMac5,2
- iMac6,1

### Mac minis
- Macmini1,1
- Macmini2,1

### Mac Pros
- MacPro1,1
- MacPro2,1

### Xserves
- Xserve1,1
- Xserve2,1

## Supported Versions of OS X
- 10.8 Mountain Lion
- 10.9 Mavericks
- 10.10 Yosemite
- 10.11 El Capitan

## Known Issues
### Graphics Acceleration, Sleep, and Brightness Control

If you're running 10.9 Mavericks or newer, or 10.10 Yosemite or newer on an AMD ATI card, your Mac won't support graphics acceleration with this patcher. Sleep and brightness control also won't work. And applications that rely on your graphics card will perform slower or will simply not work. If you have an AMD ATI card, sleep and brightness control will work fine. The system has been set to not attempt to sleep if you don't have graphics acceleration. If you would like to disable this, run sudo pmset -a disablesleep 0

Affected devices:
- All (sleep and brightness control unaffected on AMD ATI cards)

Affected versions of OS X:
- 10.9 Mavericks (AMD ATI cards unaffected)
- 10.10 Yosemite 
- 10.11 El Capitan

### Wi-Fi Connectivity

If you have a Mac mini, your Wi-Fi card will not work on Mavericks and newer. This means you'll only able to connect to the internet through Ethernet or another connection.

Affected devices:
- Macmini1,1
- Macmini2,1

Affected versions of OS X:
- 10.9 Mavericks
- 10.10 Yosemite
- 10.11 El Capitan

## Usage

### Create Installer Drive

#### Step 1

Download the latest version of the patcher from the GitHub releases page.

#### Step 2

Open Disk Utility and select your installer drive, then click Erase.

#### Step 3

Select Mac OS Extended Journaled and click Erase.

#### Step 4

Unzip the download and open Terminal. Type chmod +x and drag the script file to Terminal, then hit enter. Then type sudo, drag the script file to Terminal, and hit enter.

#### Step 5

Drag your installer app to Terminal. Then select your installer drive from the list and copy and paste the number.

### Erase System Drive (optional)

These Steps are optional. A clean install is recommended but not required. It's recommended to make a Time Machine backup before erasing your system drive.

#### Step 1

Open the Utilities menu and click Disk Utility. Select your system drive and click Erase.

#### Step 2

Select Mac OS Extended Journaled and click Erase.

### Install the OS

#### Step 1

Boot from your installer drive by holding down the option key when booting and selecting your installer drive from the menu. Then select your language from the list.

#### Step 2

Close Disk Utility if you used it to erase your system drive, then click Continue.

#### Step 3

Select your system drive, the drive to install the OS on, and click Continue.

Wait 35-45 minutes and click Reboot when the installation is complete. Then boot into your installer drive using the previous instructions.

### Patch the OS

This is the important part. This is the part where you install the patcher files onto your system. If you miss this part or forget it then your system won't boot.

#### Step 1

Open the Utilities menu and click Terminal. Type patch and hit enter. Make sure to select the model your drive will be used with. Then select your system drive from the list and copy and paste the number.

Wait 5-10 minutes and reboot when the patch tool has finished patching your system drive. Then boot into your system drive and setup the OS.

### Restore the OS

If you've switched to a new Mac or just want to remove the patcher files from your system, you can run the restore tool from your installer drive.

#### Step 1

Boot from your installer drive by holding down the option key when booting and selecting your installer drive from the menu. Then select your language from the list.

#### Step 2

Open the Utilities menu and click Terminal. Type restore and hit enter. Make sure to select the model you selected when you last patched. Then select your system drive from the list and copy and paste the number.

Wait 5-10 minutes, then reinstall the OS.

## Docs
If you want to know how OS X Patcher works, you can check [this doc](https://julianfairfax.github.io/docs/patching-macos-to-run-on-unsupported-macs.html) I wrote about patching macOS to run on unsupported Macs and [this one](https://julianfairfax.github.io/docs/patching-macos-to-install-on-unsupported-macs.html) about patching macOS to install on unsupported Macs. You can also review the code yourself and feel free to submit a pull request if you think part of it can be improved.

## License
The following files and folders were created by me and are licensed under the [GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/):
- OS X Patcher.sh
- resources/InstallerMenuAdditions.plist
- resources/patch.sh
- resources/restore.sh
- resources/patch/com.apple.PowerManagement.plist

The following files and folders were modified by me and those modificatins are licensed under the [GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/):
- resources/prelinkedkernel, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version) from Apple, from 10.11.6
- resources/patch/boot.efi, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-32-bit-firmware-problem) from Piker Alpha and Apple, from 10.10
- resources/patch/10.11/IOUSBHostFamily.kext, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-1) from Apple, from 10.10.5
- resources/patch/10.11/boot.efi, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-32-bit-firmware-problem-1) from Piker Alpha and Apple, from 10.11

The following files and folders were created by other developers and are licensed under their licenses:
- resources/patch/gmacheck, by parrotgeek1
- resources/patch/SIPManager.kext, compiled by me, from parrotgeek1
- gmacheck.c, by parrotgeek1
- SIPManager, by parrotgeek1

The following files and folders were modified by other developers and those modifications are licensed under their licenses:
- OpenGLPatch108, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-32-bit-driver-problem) by parrotgeek1, from Apple, from 10.8.5
- OpenGLPatch109, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-32-bit-driver-problem-1) by parrotgeek1, from Apple, from 10.9.5
- resources/patch/com.apple.Dock.plist, [modified](/resources/patch/com.apple.Dock.plist#L30) by parrotgeek1, from Apple, from 10.8.5
- resources/patch/OpenCL.framework, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-32-bit-driver-problem) by parrotgeek1, from Apple, from 10.8.5
- resources/patch/OpenGL.framework, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-32-bit-driver-problem) by parrotgeek1, from Apple, from 10.8.5
- resources/patch/10.9/OpenCL.framework, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-32-bit-driver-problem-1) by parrotgeek1, from Apple, from 10.9.5
- resources/patch/10.9/OpenGL.framework, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-32-bit-driver-problem-1) by parrotgeek1, from Apple, from 10.9.5
- resources/patch/10.9/CoreGraphics.framework, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-32-bit-driver-problem-1) by parrotgeek1, from Apple, from 10.9.5
- resources/patch/10.9/QuartzCore.framework, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-32-bit-driver-problem-1) by parrotgeek1, from Apple, from 10.9.5
- resources/patch/10.9/Slideshows.framework, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-32-bit-driver-problem-1) by parrotgeek1, from Apple, from 10.9.5

The following files and folders were created by Apple and are licensed under their licenses:
- resources/dm, from [10.7 recovery update](https://download.info.apple.com/Mac_OS_X/041-2768.20111012.cd14A/RecoveryHDUpdate.dmg)
- resources/patch/Accusys6xxxx.kext, from 10.8 beta 1
- resources/patch/acfsctl.kext, from 10.8 beta 1
- resources/patch/acfs.kext, from 10.8 beta 1
- resources/patch/afp.bundle, from 10.8 beta 1
- resources/patch/afpfs.kext, from 10.8 beta 1
- resources/patch/airportd, from 10.8.3
- resources/patch/AirPortUserAgent.plugin, from 10.8 beta 1
- resources/patch/ALF.kext, from 10.8 beta 1
- resources/patch/Apple16X50Serial.kext, from 10.8 beta 1
- resources/patch/Apple80211.framework, from 10.8 beta 1
- resources/patch/Apple80211Monitor.bundle, from 10.8 beta 1
- resources/patch/AppleACPIPlatform.kext, from 10.8 beta 1
- resources/patch/AppleAHCIPort.kext, from 10.8 beta 1
- resources/patch/AppleAPIC.kext, from 10.8 beta 1
- resources/patch/AppleAVBAudio.kext, from 10.8 beta 1
- resources/patch/AppleBacklightExpert.kext, from 10.8 beta 1
- resources/patch/AppleBacklight.kext, from 10.8 beta 1
- resources/patch/AppleBluetoothMultitouch.kext, from 10.8 beta 1
- resources/patch/AppleBMC.kext, from 10.8 beta 1
- resources/patch/AppleEFIRuntime.kext, from 10.8 beta 1
- resources/patch/AppleFileSystemDriver.kext, from 10.8 beta 1
- resources/patch/AppleFSCompressionTypeDataless.kext, from 10.8 beta 1
- resources/patch/AppleFSCompressionTypeZlib.kext, from 10.8 beta 1
- resources/patch/AppleFWAudio.kext, from 10.8 beta 1
- resources/patch/AppleGraphicsControl.kext, from 10.8 beta 1
- resources/patch/AppleGraphicsPowerManagement.kext, from 10.8 beta 1
- resources/patch/AppleHDA.kext, from 10.7.5
- resources/patch/AppleHIDKeyboard.kext, from 10.8 beta 1
- resources/patch/AppleHIDMouse.kext, from 10.8 beta 1
- resources/patch/AppleHPET.kext, from 10.8 beta 1
- resources/patch/AppleHWSensor.kext, from 10.8 beta 1
- resources/patch/AppleIntelCPUPowerManagementClient.kext, from 10.8 beta 1
- resources/patch/AppleIntelCPUPowerManagement.kext, from 10.8 beta 1
- resources/patch/AppleIntelFramebufferCapri.kext, from 10.8 beta 1
- resources/patch/AppleIntelGMA950GA.plugin, from 10.7.5
- resources/patch/AppleIntelGMA950GLDriver.bundle, from 10.7.5
- resources/patch/AppleIntelGMA950.kext, from 10.7.5
- resources/patch/AppleIntelGMA950VADriver.bundle, from 10.7.5
- resources/patch/AppleIntelGMAX3100FB.kext, from 10.7.5
- resources/patch/AppleIntelGMAX3100GA.plugin, from 10.7.5
- resources/patch/AppleIntelGMAX3100GLDriver.bundle, from 10.7.5
- resources/patch/AppleIntelGMAX3100.kext, from 10.7.5
- resources/patch/AppleIntelGMAX3100VADriver.bundle, from 10.7.5
- resources/patch/AppleIntelHD3000GraphicsGA.plugin, from 10.8 beta 1
- resources/patch/AppleIntelHD3000GraphicsGLDriver.bundle, from 10.8 beta 1
- resources/patch/AppleIntelHD3000Graphics.kext, from 10.8 beta 1
- resources/patch/AppleIntelHD3000GraphicsVADriver.bundle, from 10.8 beta 1
- resources/patch/AppleIntelHD4000GraphicsGA.plugin, from 10.8 beta 1
- resources/patch/AppleIntelHD4000GraphicsGLDriver.bundle, from 10.8 beta 1
- resources/patch/AppleIntelHD4000Graphics.kext, from 10.8 beta 1
- resources/patch/AppleIntelHD4000GraphicsVADriver.bundle, from 10.8 beta 1
- resources/patch/AppleIntelHDGraphicsFB.kext, from 10.8 beta 1
- resources/patch/AppleIntelHDGraphicsGA.plugin, from 10.8 beta 1
- resources/patch/AppleIntelHDGraphicsGLDriver.bundle, from 10.8 beta 1
- resources/patch/AppleIntelHDGraphics.kext, from 10.8 beta 1
- resources/patch/AppleIntelHDGraphicsVADriver.bundle, from 10.8 beta 1
- resources/patch/AppleIntelIntegratedFramebuffer.kext, from 10.7.5
- resources/patch/AppleIntelIVBVA.bundle, from 10.8 beta 1
- resources/patch/AppleIntelSNBGraphicsFB.kext, from 10.8 beta 1
- resources/patch/AppleIntelSNBVA.bundle, from 10.8 beta 1
- resources/patch/AppleIRController.kext, from 10.8 beta 1
- resources/patch/Apple_iSight.kext, from 10.8 beta 1
- resources/patch/AppleKeyStore.kext, from 10.8 beta 1
- resources/patch/AppleKeyswitch.kext, from 10.8 beta 1
- resources/patch/AppleLPC.kext, from 10.8 beta 1
- resources/patch/AppleLSIFusionMPT.kext, from 10.8 beta 1
- resources/patch/AppleMatch.kext, from 10.8 beta 1
- resources/patch/AppleMCCSControl.kext, from 10.8 beta 1
- resources/patch/AppleMCEDriver.kext, from 10.8 beta 1
- resources/patch/AppleMCP89RootPortPM.kext, from 10.8 beta 1
- resources/patch/AppleMIDIFWDriver.plugin, from 10.8 beta 1
- resources/patch/AppleMIDIIACDriver.plugin, from 10.8 beta 1
- resources/patch/AppleMIDIRTPDriver.plugin, from 10.8 beta 1
- resources/patch/AppleMIDIUSBDriver.plugin, from 10.8 beta 1
- resources/patch/AppleMikeyHIDDriver.kext, from 10.8 beta 1
- resources/patch/AppleMultitouchDriver.kext, from 10.8 beta 1
- resources/patch/ApplePlatformEnabler.kext, from 10.8 beta 1
- resources/patch/AppleProfileFamily.framework, from 10.8 beta 1
- resources/patch/AppleRAIDCard.kext, from 10.8 beta 1
- resources/patch/AppleRAID.kext, from 10.8 beta 1
- resources/patch/AppleRTC.kext, from 10.8 beta 1
- resources/patch/AppleSDXC.kext, from 10.8 beta 1
- resources/patch/AppleSEP.kext, from 10.8 beta 1
- resources/patch/AppleShareClientCore.framework, from 10.8 beta 1
- resources/patch/AppleSmartBatteryManager.kext, from 10.8 beta 1
- resources/patch/AppleSMBIOS.kext, from 10.8 beta 1
- resources/patch/AppleSMBusController.kext, from 10.8 beta 1
- resources/patch/AppleSMBusPCI.kext, from 10.8 beta 1
- resources/patch/AppleSMC.kext, from 10.8 beta 1
- resources/patch/AppleSMCLMU.kext, from 10.8 beta 1
- resources/patch/AppleSRP.kext, from 10.8 beta 1
- resources/patch/AppleStorageDrivers.kext, from 10.8 beta 1
- resources/patch/AppleThunderboltDPAdapters.kext, from 10.8 beta 1
- resources/patch/AppleThunderboltEDMService.kext, from 10.8 beta 1
- resources/patch/AppleThunderboltNHI.kext, from 10.8 beta 1
- resources/patch/AppleThunderboltPCIAdapters.kext, from 10.8 beta 1
- resources/patch/AppleThunderboltUTDM.kext, from 10.8 beta 1
- resources/patch/AppleTyMCEDriver.kext, from 10.8 beta 1
- resources/patch/AppleUpstreamUserClient.kext, from 10.8 beta 1
- resources/patch/AppleUSBAudio.kext, from 10.8 beta 1
- resources/patch/AppleUSBDisplays.kext, from 10.8 beta 1
- resources/patch/AppleUSBEthernetHost_32.kext, from 10.8 beta 1
- resources/patch/AppleUSBMultitouch.kext, from 10.8 beta 1
- resources/patch/AppleUSBTopCase.kext, from 10.8 beta 1
- resources/patch/AppleVADriver.bundle, from 10.8 beta 1
- resources/patch/AppleWWANAutoEject.kext, from 10.8 beta 1
- resources/patch/AppleXsanFilter.kext, from 10.8 beta 1
- resources/patch/ArcMSR.kext, from 10.8 beta 1
- resources/patch/asp_tcp.kext, from 10.8 beta 1
- resources/patch/ATI1300Controller.kext, from 10.8 beta 1
- resources/patch/ATI1600Controller.kext, from 10.8 beta 1
- resources/patch/ATI1900Controller.kext, from 10.8 beta 1
- resources/patch/ATI2400Controller.kext, from 10.8 beta 1
- resources/patch/ATI2600Controller.kext, from 10.8 beta 1
- resources/patch/ATI3800Controller.kext, from 10.8 beta 1
- resources/patch/ATI4600Controller.kext, from 10.8 beta 1
- resources/patch/ATI4800Controller.kext, from 10.8 beta 1
- resources/patch/ATI5000Controller.kext, from 10.8 beta 1
- resources/patch/ATI6000Controller.kext, from 10.8 beta 1
- resources/patch/ATIFramebuffer.kext, from 10.8 beta 1
- resources/patch/ATIRadeonX1000GA.plugin, from 10.8 beta 1
- resources/patch/ATIRadeonX1000GLDriver.bundle, from 10.8 beta 1
- resources/patch/ATIRadeonX1000.kext, from 10.8 beta 1
- resources/patch/ATIRadeonX1000VADriver.bundle, from 10.8 beta 1
- resources/patch/ATIRadeonX2000GA.plugin, from 10.8 beta 1
- resources/patch/ATIRadeonX2000GLDriver.bundle, from 10.8 beta 1
- resources/patch/ATIRadeonX2000.kext, from 10.8 beta 1
- resources/patch/ATIRadeonX2000VADriver.bundle, from 10.8 beta 1
- resources/patch/ATIRadeonX3000GA.plugin, from 10.8 beta 1
- resources/patch/ATIRadeonX3000GLDriver.bundle, from 10.8 beta 1
- resources/patch/ATIRadeonX3000.kext, from 10.8 beta 1
- resources/patch/ATIRadeonX3000VADriver.bundle, from 10.8 beta 1
- resources/patch/ATISupport.kext, from 10.8 beta 1
- resources/patch/ATTOCelerityFC8.kext, from 10.8 beta 1
- resources/patch/ATTOCelerityFC.kext, from 10.8 beta 1
- resources/patch/ATTOExpressPCI4.kext, from 10.8 beta 1
- resources/patch/ATTOExpressSASHBA2.kext, from 10.8 beta 1
- resources/patch/ATTOExpressSASHBA.kext, from 10.8 beta 1
- resources/patch/ATTOExpressSASRAID2.kext, from 10.8 beta 1
- resources/patch/ATTOExpressSASRAID.kext, from 10.8 beta 1
- resources/patch/AudioAUUC.kext, from 10.8 beta 1
- resources/patch/autofs.kext, from 10.8 beta 1
- resources/patch/AVRCPAgent.app, from 10.8 beta 1
- resources/patch/BJUSBLoad.kext, from 10.8 beta 1
- resources/patch/blued, from 10.8 beta 1
- resources/patch/BluetoothAudioAgent.app, from 10.8 beta 1
- resources/patch/Bluetooth.menu, from 10.8 beta 1
- resources/patch/Bluetooth.prefPane, from 10.8 beta 1
- resources/patch/Bluetooth Setup Assistant.app, from 10.8 beta 1
- resources/patch/BluetoothUserAgent-Plugin.plugin, from 10.8 beta 1
- resources/patch/bnepd, from 10.8 beta 1
- resources/patch/BootCache.kext, from 10.8 beta 1
- resources/patch/CalDigitHDProDrv.kext, from 10.8 beta 1
- resources/patch/cd9660.kext, from 10.8 beta 1
- resources/patch/cddafs.kext, from 10.8 beta 1
- resources/patch/CellPhoneHelper.kext, from 10.8 beta 1
- resources/patch/CoreStorage.kext, from 10.8 beta 1
- resources/patch/CoreWiFi.framework, from 10.8 beta 1
- resources/patch/CoreWLAN.framework, from 10.8 beta 1
- resources/patch/CoreWLANKit.framework, from 10.8 beta 1
- resources/patch/Dont Steal Mac OS X.kext, from 10.8 beta 1
- resources/patch/DSACL.ppp, from 10.8 beta 1
- resources/patch/DSAuth.ppp, from 10.8 beta 1
- resources/patch/DVFamily.bundle, from 10.8 beta 1
- resources/patch/EAP8021X.framework, from 10.8 beta 1
- resources/patch/EAP-KRB.ppp, from 10.8 beta 1
- resources/patch/EAPOLController.bundle, from 10.8 beta 1
- resources/patch/EAP-RSA.ppp, from 10.8 beta 1
- resources/patch/EAP-TLS.ppp, from 10.8 beta 1
- resources/patch/EPSONUSBPrintClass.kext, from 10.8 beta 1
- resources/patch/exfat.kext, from 10.8 beta 1
- resources/patch/GeForce7xxxGA.plugin, from 10.7.5
- resources/patch/GeForce7xxxGLDriver.bundle, from 10.7.5
- resources/patch/GeForce7xxx.kext, from 10.7.5
- resources/patch/GeForce7xxxVADriver.bundle, from 10.7.5
- resources/patch/GeForceGA.plugin, from 10.7.5
- resources/patch/GeForceGLDriver.bundle, from 10.7.5
- resources/patch/GeForce.kext, from 10.7.5
- resources/patch/GeForceVADriver.bundle, from 10.7.5
- resources/patch/hfs.fs, from 10.8 beta 1
- resources/patch/HighPointIOP.kext, from 10.8 beta 1
- resources/patch/HighPointRR644.kext, from 10.8 beta 1
- resources/patch/HighPointRR.kext, from 10.8 beta 1
- resources/patch/hp_fax_io.kext, from 10.8 beta 1
- resources/patch/hp_Inkjet1_io_enabler.kext, from 10.8 beta 1
- resources/patch/IO80211Family.kext, from 10.8 beta 1
- resources/patch/IOAccelerator2D.plugin, from 10.8 beta 1
- resources/patch/IOAcceleratorFamily.kext, from 10.8 beta 1
- resources/patch/IOACPIFamily.kext, from 10.8 beta 1
- resources/patch/IOAHCIFamily.kext, from 10.8 beta 1
- resources/patch/IOATAFamily.kext, from 10.8 beta 1
- resources/patch/IOAudioFamily.kext, from 10.8.4
- resources/patch/IOAVBFamily.kext, from 10.8 beta 1
- resources/patch/IOBDStorageFamily.kext, from 10.8 beta 1
- resources/patch/IOBluetoothFamily.kext, from 10.8 beta 1
- resources/patch/IOBluetooth.framework, from 10.8 beta 1
- resources/patch/IOBluetoothHIDDriver.kext, from 10.8 beta 1
- resources/patch/IOCDStorageFamily.kext, from 10.8 beta 1
- resources/patch/IODVDStorageFamily.kext, from 10.8 beta 1
- resources/patch/IOFireWireAVC.kext, from 10.8 beta 1
- resources/patch/IOFireWireFamily.kext, from 10.8 beta 1
- resources/patch/IOFireWireIP.kext, from 10.8 beta 1
- resources/patch/IOFireWireSBP2.kext, from 10.8 beta 1
- resources/patch/IOFireWireSerialBusProtocolTransport.kext, from 10.8 beta 1
- resources/patch/IOGraphicsFamily.kext, from 10.8 beta 1
- resources/patch/IOHDIXController.kext, from 10.8 beta 1
- resources/patch/IOHIDFamily.kext, from 10.8 beta 1
- resources/patch/IONDRVSupport.kext, from 10.8 beta 1
- resources/patch/IONetworkingFamily.kext, from 10.8 beta 1
- resources/patch/IOPCIFamily.kext, from 10.8 beta 1
- resources/patch/IOPlatformPluginFamily.kext, from 10.8 beta 1
- resources/patch/IOSCSIArchitectureModelFamily.kext, from 10.8 beta 1
- resources/patch/IOSCSIParallelFamily.kext, from 10.8 beta 1
- resources/patch/IOSerialFamily.kext, from 10.8 beta 1
- resources/patch/IOSMBusFamily.kext, from 10.8 beta 1
- resources/patch/IOStorageFamily.kext, from 10.8 beta 1
- resources/patch/IOStreamFamily.kext, from 10.8 beta 1
- resources/patch/IOSurface.kext, from 10.8 beta 1
- resources/patch/IOThunderboltFamily.kext, from 10.8 beta 1
- resources/patch/IOTimeSyncFamily.kext, from 10.8 beta 1
- resources/patch/IOUSBAttachedSCSI.kext, from 10.8 beta 1
- resources/patch/IOUSBFamily.kext, from 10.8 beta 1
- resources/patch/IOUSBMassStorageClass.kext, from 10.8 beta 1
- resources/patch/IOUserEthernet.kext, from 10.8 beta 1
- resources/patch/IOVideoFamily.kext, from 10.8 beta 1
- resources/patch/iPodDriver.kext, from 10.8 beta 1
- resources/patch/JMicronATA.kext, from 10.8 beta 1
- resources/patch/kernel, from 10.11.6 final
- resources/patch/L2TP.ppp, from 10.8 beta 1
- resources/patch/mach_kernel, from 10.8 beta 1
- resources/patch/mcxalr.kext, from 10.8 beta 1
- resources/patch/mDNSResponder, from 10.8 beta 1
- resources/patch/Mouse.prefPane, from 10.8 beta 1
- resources/patch/msdosfs.kext, from 10.8 beta 1
- resources/patch/NetFS.framework, from 10.8 beta 1
- resources/patch/ntfs.kext, from 10.8 beta 1
- resources/patch/NVDAGF100Hal.kext, from 10.7.5
- resources/patch/NVDAGK100Hal.kext, from 10.7.5
- resources/patch/NVDANV40HalG7xxx.kext, from 10.7.5
- resources/patch/NVDANV50Hal.kext, from 10.7.5
- resources/patch/NVDAResmanG7xxx.kext, from 10.7.5
- resources/patch/NVDAResman.kext, from 10.7.5
- resources/patch/NVSMU.kext, from 10.8 beta 1
- resources/patch/OSvKernDSPLib.kext, from 10.8 beta 1
- resources/patch/PPP.kext, from 10.8 beta 1
- resources/patch/PPPoE.ppp, from 10.8 beta 1
- resources/patch/PPPSerial.ppp, from 10.8 beta 1
- resources/patch/PPTP.ppp, from 10.8 beta 1
- resources/patch/PromiseSTEX.kext, from 10.8 beta 1
- resources/patch/Quarantine.kext, from 10.8 beta 1
- resources/patch/Radius.ppp, from 10.8 beta 1
- resources/patch/SM56KUSBAudioFamily.kext, from 10.8 beta 1
- resources/patch/SMARTLib.plugin, from 10.8 beta 1
- resources/patch/smbfs.kext, from 10.8 beta 1
- resources/patch/SMCMotionSensor.kext, from 10.8 beta 1
- resources/patch/SoftRAID.kext, from 10.8 beta 1
- resources/patch/SPBluetoothReporter.spreporter, from 10.8 beta 1
- resources/patch/SPDisplaysReporter.spreporter, from 10.8 beta 1
- resources/patch/System.kext, from 10.8 beta 1
- resources/patch/TMSafetyNet.kext, from 10.8 beta 1
- resources/patch/triggers.kext, from 10.8 beta 1
- resources/patch/udf.kext, from 10.8 beta 1
- resources/patch/webcontentfilter.kext, from 10.8 beta 1
- resources/patch/webdav_fs.kext, from 10.8 beta 1
- resources/patch/wifid, from 10.8 beta 1
- resources/patch/wps, from 10.8 beta 1
- resources/patch/10.9/AppleIntelGMA950GA.plugin, from 10.6.2 beta 1
- resources/patch/10.9/AppleIntelGMA950GLDriver.bundle, from 10.6.2 beta 1
- resources/patch/10.9/AppleIntelGMA950.kext, from 10.6.2 beta 1
- resources/patch/10.9/AppleIntelGMA950VADriver.bundle, from 10.6.2 beta 1
- resources/patch/10.9/AppleIntelGMAX3100FB.kext, from 10.6.2 beta 1
- resources/patch/10.9/AppleIntelGMAX3100GA.plugin, from 10.6.2 beta 1
- resources/patch/10.9/AppleIntelGMAX3100GLDriver.bundle, from 10.6.2 beta 1
- resources/patch/10.9/AppleIntelGMAX3100.kext, from 10.6.2 beta 1
- resources/patch/10.9/AppleIntelGMAX3100VADriver.bundle, from 10.6.2 beta 1
- resources/patch/10.9/AppleIntelIntegratedFramebuffer.kext, from 10.6.2 beta 1
- resources/patch/10.9/GeForce7xxxGLDriver.bundle, from 10.6.2 beta 1
- resources/patch/10.9/GeForce8xxxGLDriver.bundle, from 10.6.2 beta 1
- resources/patch/10.9/GeForceGA.plugin, from 10.6.2 beta 1
- resources/patch/10.9/GeForce.kext, from 10.6.2 beta 1
- resources/patch/10.9/GeForceVADriver.bundle, from 10.6.2 beta 1
- resources/patch/10.9/NVDANV40Hal.kext, from 10.6.2 beta 1
- resources/patch/10.9/NVDANV50Hal.kext, from 10.6.2 beta 1
- resources/patch/10.9/NVDAResman.kext, from 10.6.2 beta 1
- resources/patch/10.11/AppleHIDMouse.kext, from 10.10.5
- resources/patch/10.11/AppleIRController.kext, from 10.7.5
- resources/patch/10.11/AppleTopCase.kext, from 10.10.5
- resources/patch/10.11/AppleUSBMultitouch.kext, from 10.10.5
- resources/patch/10.11/AppleUSBTopCase.kext, from 10.10.5
- resources/patch/10.11/IOBDStorageFamily.kext, from 10.10.5
- resources/patch/10.11/IOBluetoothFamily.kext, from 10.10.5
- resources/patch/10.11/IOBluetoothHIDDriver.kext, from 10.10.5
- resources/patch/10.11/IOSerialFamily.kext, from 10.10.5
- resources/patch/10.11/IOUSBFamily.kext, from 10.10.5
- resources/patch/10.11/IOUSBMassStorageClass.kext, from 10.10.5
